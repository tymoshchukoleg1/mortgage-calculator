const Koa = require('koa');
const cors = require('@koa/cors');

const bodyParser = require('koa-bodyparser');
const bankRoutes = require('./db/routes/banks.routes');

const app = new Koa();
app.use(cors());

app.use(bodyParser());
app.use(bankRoutes.routes()).use(bankRoutes.allowedMethods());

const port = process.env.PORT || 3000;

app.listen(port);
console.log("Application is running on port 3000");
