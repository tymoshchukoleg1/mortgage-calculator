const Router = require('koa-router');

const {createBank, getBanks, deleteBank, updateBank, getBank} = require('../api/banks.api');

const router = new Router({
    prefix:'/api/banks'
});

router.get('/', async(ctx, next) => {
    ctx.body = await getBanks();
});

router.post ('/', async (ctx, next) => {
    let bank = ctx.request.body;
    bank = await createBank(bank);
    ctx.response.status = 200;
    ctx.body = bank;
})

router.get('/:id', async (ctx, next) => {
    const id = ctx.params.id;
    ctx.body = await getBank(id);
})

router.delete('/:id', async (ctx, next) => {
    const id = ctx.params.id;
    await deleteBank(id);
    ctx.response.status = 200;
})

router.put('/:id', async (ctx, next) => {
    const id = ctx.params.id;
    let bank = ctx.request.body;
    bank = await updateBank(id, bank);
    ctx.response.status = 200;
    ctx.body = bank;
})

module.exports = router;
