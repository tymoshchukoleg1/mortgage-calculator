const banks = require('./index').db('mortgage-calculator').collection('banks');

const ObjectId = require('mongodb').ObjectId;

const save = async ({bankName, interestRate, maxLoan, minPayment, loanTerm}) => {
    const result = await banks.insertOne({bankName, interestRate, maxLoan, minPayment, loanTerm});
    return result;
}

const getAll = async () => {
    const cursor = await banks.find();
    return cursor.toArray();
}

const getById = async (id) => {
    return await banks.findOne({_id:ObjectId(id)});
}

const update = async (id, {bankName, interestRate, maxLoan, minPayment, loanTerm}) => {
    const result = await banks.replaceOne({_id:ObjectId(id)}, {bankName, interestRate, maxLoan, minPayment, loanTerm});
    return result;
}

const removeById = async (id) => {
    await banks.deleteOne({_id:ObjectId(id)});
}

module.exports = {save, getAll, update, getById, removeById};
