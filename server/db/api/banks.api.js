const {save, getAll, update, getById, removeById} = require('../banks.crud');

const createBank = async ({bankName, interestRate, maxLoan, minPayment, loanTerm}) => {
    const bank = {
        bankName,
        interestRate,
        maxLoan,
        minPayment,
        loanTerm
    }

    return await save(bank);
}

const getBanks = async () => {
    return await getAll();
}

const deleteBank =  async (id) => {
    return await removeById(id);
}

const getBank = async (id) => {
    return await getById(id);
}

const updateBank = async (id, {bankName, interestRate, maxLoan, minPayment, loanTerm}) => {
    return await update(id, {bankName, interestRate, maxLoan, minPayment, loanTerm});
}

module.exports = {
    createBank,
    getBanks,
    deleteBank,
    updateBank,
    getBank
}
