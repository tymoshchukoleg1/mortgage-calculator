# Mortgage calculator

> A fullstack app using Vue.js, Koa and MongoDB. You will need to edit the MongoDB connection string in server/db/app.js to your own.

## Quick Start

```bash
# Install dependencies
npm install

# Start Koa Server: http://localhost:3000
npm start

# Start Vue DevServer: http://localhost:8080
cd client
npm install
npm run serve
```
### Author

Tymoshchuk Oleh

### Version

1.0.0
