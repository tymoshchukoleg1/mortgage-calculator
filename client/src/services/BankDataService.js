const axios = require('axios');

const http = axios.create({
  baseURL: "api/banks",
  headers: {
    "Content-type": "application/json"
  }
})

class BankDataService {
  getAll() {
    const result = http.get('/')
    return result;
  }
  create(data) {
    return http.post("/", data);
  }
  update(id, data) {
    return http.put(`/${id}`, data);
  }
  delete(id) {
    return http.delete(`/${id}`);
  }
}

export default new BankDataService();
