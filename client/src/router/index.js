import Vue from 'vue';
import VueRouter from 'vue-router';
import Calculator from '../components/Calculator.vue';
import BankList from '../components/BankList.vue';


Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    alias: '/banks',
    name: 'banks',
    component: BankList,
  },
  {
    path: '/calculator',
    name: 'calculator',
    component: Calculator,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
